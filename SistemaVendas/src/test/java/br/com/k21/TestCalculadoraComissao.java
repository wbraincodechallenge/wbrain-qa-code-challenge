package br.com.k21;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

public class TestCalculadoraComissao {

	@Test
	public void TesteComissao() {
		int valor = 1000;
		int valorEsperado = 50;
		int retorno = CalculadoraComissao.Calcular(1000);

		Assert.assertEquals(valorEsperado, retorno, 0);
	}
}
